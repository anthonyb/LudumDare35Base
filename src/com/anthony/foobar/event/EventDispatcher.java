package com.anthony.foobar.event;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;

import com.anthony.foobar.FooBar;

public class EventDispatcher {

	private Collection<Object> handles = new ArrayList<Object>();
	
	public void addHandler(Object handler) {
		
		this.handles.add(handler);
	}
	
	public void removeHandler(Object handler) {
		
		this.handles.remove(handler);
	}
	
	public void dispatchEvent(Event event) {
		
		for (Object handler : this.handles) {
			
			this.dispatchEventTo(event, handler);
		}
	}
	
//	TODO implement Listen priority
	protected void dispatchEventTo(Event event, Object handler) {
		
		Method[] methods = handler.getClass().getDeclaredMethods();
		
		for (int i = 0; i < methods.length; i++) {
			
			Method m = methods[i];
			
			Listen listen = m.getAnnotation(Listen.class);
			
			if (listen == null) {
				
				continue;
			}
			
			Class<?>[] methodParams = m.getParameterTypes();
			
			if (methodParams.length != 1) {
				
				continue;
			}
			
			if (!event.getClass().getSimpleName().equals(methodParams[0].getSimpleName())) {
				
				continue;
			}
			
			try {
				
				m.setAccessible(true);
				m.invoke(handler, event);
			} catch (Exception e) {
				
				FooBar.getLogger().warn("Could not invoke event handler!");
				e.printStackTrace();
			}
		}
	}
}