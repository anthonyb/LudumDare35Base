package com.anthony.foobar.threed.camera;

import com.anthony.foobar.threed.math.Matrix;
import com.anthony.foobar.threed.point.Point3D;

public class Camera {
	
	private Point3D point;
	
//	TODO angle x angle y angle z
	private double angleX, angleY, angleZ, screenWidth, screenHeight;
	
	public Matrix cameraMatrix;
	
	public Camera(double x, double y, double z, double screenWidth, double screenHeight) {
		
		this.point = new Point3D(x, y, z);
		
		this.screenWidth = screenWidth;
		this.screenHeight = screenHeight;
		
		this.cameraMatrix = new Matrix(
				
			new double[][] {
					
					new double[]{1, 0, 0, -this.point.getX()},
					new double[]{0, 1, 0, -this.point.getY()},
					new double[]{0, 0, 1, -this.point.getZ()},
					new double[]{0, 0, 0, 1}
			}
		);
		
		this.angleX = 0;
		this.angleY = 0;
		this.angleZ = 0;
	}
	
	public double getX() {
		
		return this.point.getX();
	}
	
	public double getY() {
		
		return this.point.getY();
	}
	
	public double getZ() {
		
		return this.point.getZ();
	}
	
	public Point3D getPoint() {
		
		return this.point;
	}
	
	public double getAngleX() {
		
		return this.angleX * 180 / Math.PI;
	}
	
	public double getAngleY() {
		
		return this.angleY * 180 / Math.PI;
	}
	
	public double getAngleZ() {
		
		return this.angleZ * 180 / Math.PI;
	}
	
	public double getScreenWidth() {
		
		return this.screenWidth;
	}
	
	public double getHalfScreenWidth() {
		
		return .5 * this.screenWidth;
	}
	
	public double getScreenHeight() {
		
		return this.screenHeight;
	}
	
	public double getHalfScreenHeight() {
		
		return .5 * this.screenHeight;
	}
	
	public void setX(double x) {
		
		this.point.setX(x);
		
		this.cameraMatrix.data[0][3] = -this.point.getX();
	}
	
	public void setY(double y) {
		
		this.point.setY(y);
		
		this.cameraMatrix.data[1][3] = -this.point.getY();
	}
	
	public void setZ(double z) {
		
		this.point.setZ(z);
		
		this.cameraMatrix.data[2][3] = -this.point.getZ();
	}
	
	public void setAngleX(double degrees) {
		
		this.angleX = (degrees % 360 * Math.PI) / 180.0; 
	}
	
	public void setAngleY(double degrees) {
		
		this.angleY = degrees % 360 * Math.PI / 180.0;
	}
	
	public void setAngleZ(double degrees) {
		
		this.angleZ = degrees % 360 * Math.PI / 180.0;
	}
}