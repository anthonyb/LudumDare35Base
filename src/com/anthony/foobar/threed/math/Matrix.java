package com.anthony.foobar.threed.math;

/**
 * Matrix class.
 * @author http://introcs.cs.princeton.edu/java/95linear/Matrix.java.html
 */
public class Matrix {

	public final int M; //rows
	public final int N; //columns
	
	public double[][] data; //M by N array
	
	public Matrix(double[][] data) {
		
		this.M = data.length;
		this.N = data[0].length; //every row has the same amount of columns in a matrix
		
		this.data = data;
	}
	
	public Matrix(int M, int N) {
		
		this.M = M;
		this.N = N;
		
		this.data = new double[M][N];
	}
	
//	return C = A * B
	public Matrix multiply(Matrix B) {
		
		Matrix A = this;
		
		if (A.N != 1) {
			
			throw new RuntimeException("Multiply requires an Mx1 matrix!");
		}
		
		Matrix C = new Matrix(A.M, 1);
		
		for (int i = 0; i < B.data.length; i++) {
			
			double sum = 0;
			
			for (int j = 0; j < B.data[i].length; j++) {
				
				sum += (A.data[j][0] * B.data[i][j]);
			}
			
			C.data[i] = new double[]{sum};
		}
		
		return C;
	}
}