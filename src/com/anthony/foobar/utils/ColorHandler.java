package com.anthony.foobar.utils;

public class ColorHandler {
	
	public static int changeBrightness(int col, int amount) {
		
		int r = (col & 0xff0000) >> 16;
		int g = (col & 0xff00) >> 8;
		int b = (col & 0xff);
		
		if(amount < -180) {
			
			amount = -180;
		} else if(amount > 0) {
			
			amount = 0;
		}
		
		r += amount;
		g += amount;
		b += amount;
		
		if (r < 0) {
			
			r = 0;
		} else if (r > 255) {
			
			r = 255;
		}
		
		if (g < 0) {
			
			g = 0;
		} else if (g > 255) {
			
			g = 255;
		}
		
		if (b < 0) {
			
			b = 0;
		} else if (b > 255) {
			
			b = 255;
		}
		
		return r << 16 | g << 8 | b;
	}
	
	public static int negative(int col) {
		
		return (0x00FFFFFF - (col | 0xFF000000)) | (col & 0xFF000000);
	}
	
	public static int blend (int color1, int color2, float ratio) {
		
		if (ratio > 1f) {
			
			ratio = 1f;
		} else if (ratio < 0f) {
			
			ratio = 0f;
		}
		
		float inverseRatio = 1.0f - ratio;
		
		int r1 = (color1 & 0xff0000) >> 16;
		int g1 = (color1 & 0xff00) >> 8;
		int b1 = (color1 & 0xff);
		
		int r2 = (color2 & 0xff0000) >> 16;
		int g2 = (color2 & 0xff00) >> 8;
		int b2 = (color2 & 0xff);
		
		int r = ((int)(r1 * inverseRatio) + (int)(r2 * ratio));
		int g = ((int)(g1 * inverseRatio) + (int)(g2 * ratio));
		int b = ((int)(b1 * inverseRatio) + (int)(b2 * ratio));
		
		return r << 16 | g << 8 | b;
	}
}