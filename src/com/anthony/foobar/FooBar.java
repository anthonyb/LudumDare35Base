package com.anthony.foobar;

import com.anthony.foobar.event.EventDispatcher;
import com.anthony.foobar.logger.Logger;

/**
 * <h1>
 * <u>Welcome to FooBar!</u><br/>
 * <i>For help read the "README" below.</i>
 * </h1>
 * <br/>
 * <p>
 * <b><u><i>README:</i></u></b><br/>
 * <br/>
 * Initialize FooBar by calling the init method:
 * <center>{@code FooBar.init(...loggerFolderLoc..., ...loggerFolderName...)}</center><br/>
 * 
 * After initializing do the following:
 * <li>Register objects that use the {@code @Listener} annotation by calling the addHandler method:
 * <center>{@code FooBar.getEventDispatcher().addHandler(...object...);}</center>
 * </p>
 */
public class FooBar {
	
	private static Logger logger;
	
	private static EventDispatcher eventDispatcher;
	
	/**
	 * Initializes FooBar.
	 * @param loggerFolderLoc Location for a {@code Logger} folder to be created.
	 * @param loggerFolderName Name of the {@code Logger} folder.
	 */
	public static void init(String loggerFolderLoc, String loggerFolderName) {
		
		FooBar.logger = new Logger(loggerFolderLoc, loggerFolderName);
		
		FooBar.eventDispatcher = new EventDispatcher();
		FooBar.eventDispatcher.addHandler(FooBar.logger);
		
		Runtime.getRuntime().addShutdownHook(new ShutdownHook());
	}
	
	public static Logger getLogger() {
		
		return FooBar.logger;
	}
	
	public static EventDispatcher getEventDispatcher() {
		
		return FooBar.eventDispatcher;
	}
}