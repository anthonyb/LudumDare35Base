package com.anthony.foobar.twod.point;

public class Point2D {
	
	public double x, y;
	
	public Point2D(double x, double y) {
		
		this.x = x;
		this.y = y;
	}
	
	public void subtract(Point2D point) {
		
		this.x -= point.x;
		this.y -= point.y;
	}
	
	public void add(Point2D point) {
		
		this.x += point.x;
		this.y += point.y;
	}
	
	public static Point2D subtract(Point2D p1, Point2D p2) {
		
		return new Point2D(p1.x - p2.x, p1.y - p2.y);
	}
	
	public static Point2D add(Point2D p1, Point2D p2) {
		
		return new Point2D(p1.x + p2.x, p1.y + p2.y);
	}
}