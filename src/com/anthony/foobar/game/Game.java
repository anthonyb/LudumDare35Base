package com.anthony.foobar.game;

import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.event.KeyListener;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;

import javax.swing.JFrame;

import com.anthony.foobar.game.graphics.screen.Screen;
import com.anthony.foobar.threed.camera.Camera;

/**
 * TODO
 * TODOs in this class
 * TODOs in Screen class
 */
public abstract class Game {
	
	private boolean running;
	
	private int width, height, maxTPS, fps, ticks;
	
	private String title;
	
	private JFrame frame;
	private Canvas canvas;
	
	private BufferStrategy bs;
	private Graphics g;
	
	private Camera camera;
	
	private Screen screen;
	
	private BufferedImage image;
	private int[] pixels;
	
	public Game(int width, int height, int maxTPS, String title) {
		
		this.width = width;
		this.height = height;
		this.maxTPS = maxTPS;
		this.fps = this.ticks = 0;
		
		this.title = title;
		
		this.frame = new JFrame();
		
		this.frame.setResizable(false);
		
		this.canvas = new Canvas();
		
		this.canvas.setSize(this.width, this.height);
		
		this.frame.add(this.canvas);
		
		this.frame.pack();
		
		this.canvas.createBufferStrategy(3);
		
		this.frame.setTitle(this.title);
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frame.setLocationRelativeTo(null);
		this.frame.setVisible(true);
		this.frame.requestFocus();
		
		this.camera = new Camera(0, 0, 1, this.width, this.height);
		
		this.screen = new Screen(this);
		
		this.image = new BufferedImage(this.width, this.height, BufferedImage.TYPE_INT_RGB);
		this.pixels = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();
		
		this.run();
	}
	
	protected void addKeyListener(KeyListener keyListener) {
		
		this.canvas.addKeyListener(keyListener);
	}
	
	private void run() {
		
		this.running = true;
		
		this.bs = this.canvas.getBufferStrategy();
		
		long lastTime = System.nanoTime();
		double nsPerTick = 1_000_000_000D / this.maxTPS;
		
		long lastTimer = System.currentTimeMillis();
		double delta = 0D;
		
		int fps_ = 0;
		int ticks_ = 0;
		
		while (this.running) {
			
			long now = System.nanoTime();
			delta += (now - lastTime) / nsPerTick;
			lastTime = now;
			
			while (delta >= 1) {
				
				this.tick();
				ticks_++;
				
				delta--;
			}
			
			this.render();
			fps_++;
			
			if (System.currentTimeMillis() - lastTimer >= 1_000) {
				
				this.fps = fps_;
				this.ticks = ticks_;
				
				fps_ = ticks_ = 0;
				
				lastTimer += 1_000;
			}
		}
	}
	
	protected void tick() {
		
		this.screen.tick();
		
		this.onTick();
	}
	
	protected void render() {
		
		this.g = this.bs.getDrawGraphics();
		
		this.onRender();
//		TODO this.level.render()
		
		int[] screenPixels = this.screen.getPixels();
		
		for (int i = 0; i < this.pixels.length; i++) {
			
			this.pixels[i] = screenPixels[i];
		}
		
		this.g.drawImage(this.image, 0, 0, this.width, this.height, null);
		
		this.screen.clear();
		
		this.g.dispose();
		this.bs.show();
	}
	
	public abstract void onTick();
	
	public abstract void onRender();
	
	public int getWidth() {
		
		return this.width;
	}
	
	public int getHeight() {
		
		return this.height;
	}
	
	public int getFPS() {
		
		return this.fps;
	}
	
	public int getTicks() {
		
		return this.ticks;
	}
	
	public Camera getCamera() {
		
		return this.camera;
	}
}
