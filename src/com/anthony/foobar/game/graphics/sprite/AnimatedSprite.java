package com.anthony.foobar.game.graphics.sprite;

public class AnimatedSprite {
	
	private int frame, time, rate, length;
	
	private boolean reverseAtEnd, rising;
	
	private Sprite[] sprites;
	
	public AnimatedSprite(Sprite... sprites) {
		
		this(5, true, sprites);
	}
	
	public AnimatedSprite(int rate, boolean reverseAtEnd, Sprite... sprites) {
		
		this.frame = this.time = 0;
		this.rate = rate;
		this.length = sprites.length - 1;
		
		this.reverseAtEnd = reverseAtEnd;
		this.rising = true;
		
		this.sprites = sprites;
	}
	
	public void update() {
		
		time++;
		
		if (time % rate == 0) {
			
			if (this.reverseAtEnd) {
				
				if (this.rising) {
					
					if (this.frame == this.length) {
						
						this.frame = this.frame - 1;
						this.rising = false;
					} else {
					
						this.frame = this.frame + 1;
					}
				} else {
					
					if (this.frame == 0) {
						
						this.frame = 1;
						this.rising = true;
					} else {
						
						this.frame = this.frame - 1;
					}
				}
			} else {
				
				this.frame = this.frame == this.length ? 0 : this.frame + 1;
			}
			
			if (time > 1_000) {
				
				this.time = 0;
			}
		}
	}
	
	public void setFrame(int frame) {
		
		if (frame < 0) {
			
			frame = 0;
		}
		
		if (frame > this.length) {
			
			frame = this.length;
		}
		
		this.frame = frame;
	}
	
	public void setSprites(Sprite... sprites) {
		
		this.sprites = sprites;
	}
	
	public int getFrame() {
		
		return this.frame;
	}
	
	public Sprite getSprite() {
		
		return this.sprites[this.frame];
	}
}