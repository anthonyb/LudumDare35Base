package com.anthony.foobar.game.graphics.sprite;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.anthony.foobar.FooBar;

public class SpriteSheet {
	
	public int width;
	public int height;
	
	public int[] pixels;
	
	private String path;
	
	public SpriteSheet(String path) {
		
		this.path = path;
		
		this.load();
	}
	
	public void load() {
		
		try {
			
			BufferedImage image = ImageIO.read(SpriteSheet.class.getResource(this.path));
			this.width = image.getWidth();
			this.height = image.getHeight();
			this.pixels = new int[this.width * this.height];
			
			image.getRGB(0, 0, this.width, this.height, this.pixels, 0, this.width);
		} catch (IOException e) {
			
			e.printStackTrace();
			
			FooBar.getLogger().warn("Could not load resource " + this.path + "!");
			FooBar.getLogger().info("Shutting down...");
			
			System.exit(0);
		}
	}
}