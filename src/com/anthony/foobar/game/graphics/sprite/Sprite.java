package com.anthony.foobar.game.graphics.sprite;

import java.awt.Color;

public class Sprite {
	
	private int x, y, width, height;
	
	public int[] pixels;
	
	private SpriteSheet sheet;
	
	public Sprite(int x, int y, int width, int height, SpriteSheet sheet) {
		
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		
		this.pixels = new int[width * height];
		
		this.sheet = sheet;
		
		this.load();
	}
	
	public Sprite(Sprite sprite, Color[] greyscale, Color[] colored) {
		
		this.x = sprite.x;
		this.y = sprite.y;
		this.width = sprite.getWidth();
		this.height = sprite.getHeight();
		
		this.pixels = new int[width * height];
		
		this.sheet = sprite.getSpriteSheet();
		
		this.load(greyscale, colored);
	}
	
	public Sprite(Sprite sprite, boolean rotDown, boolean rotRight, boolean rotLeft) {
		
		this.x = sprite.x;
		this.y = sprite.y;
		this.width = sprite.width;
		this.height = sprite.height;
		
		this.sheet = sprite.sheet;
		
		this.pixels = sprite.pixels.clone();
		
		int[][] matrix = new int[this.height][this.width];
		
		for (int i = 0; i < this.height; i++) {
			
			for (int j = 0; j < this.width; j++) {
				
				matrix[i][j] = this.pixels[(i * this.width) + j];
			}
		}
		
		if (rotRight || rotDown) {
			
			/*
			 * NOTE: Is the system backwards?
			 * I rotate left to go right, and rotate right to go left.
			 * TODO investigate.
			 *
			 * [0,0] [1,0] [2,0] [3,0]
			 * [0,1] [1,1] [2,1] [3,1]
			 * [0,2] [1,2] [2,2] [3,2]
			 * [0,3] [1,3] [2,3] [3,3]
			 * 
			 * [3,0] [3,1] [3,2] [3,3]
			 * [2,0] [2,1] [2,2] [2,3]
			 * [1,0] [1,1] [1,2] [1,3]
			 * [0,0] [0,1] [0,2] [0,3]
			 */
			
			for (int repeat = 0; repeat < (rotDown ? 2 : 1); repeat++) {
				
				int[][] matrixClone = new int[this.height][this.width];
				
				for (int i = 0; i < this.height; i++) {
					
					for (int j = 0; j < this.width; j++) {
						
						matrixClone[i][j] = matrix[i][j];
					}
				}
				
				for (int i = 0; i < this.height; i++) {
					
					for (int j = 0; j < this.width; j++) {
						
						int newY = this.height - (j + 1);
						int newX = i;
						
						matrix[i][j] = matrixClone[newY][newX];
					}
				}
			}
		}
		
		if (rotLeft) {
			
			/*
			 * [0,0] [1,0] [2,0] [3,0]
			 * [0,1] [1,1] [2,1] [3,1]
			 * [0,2] [1,2] [2,2] [3,2]
			 * [0,3] [1,3] [2,3] [3,3]
			 * 
			 * [0,3] [0,2] [0,1] [0,0]
			 * [1,3] [1,2] [1,1] [1,0]
			 * [2,3] [2,2] [2,1] [2,0]
			 * [3,3] [3,2] [3,1] [3,0]
			 */
			
			int[][] matrixClone = new int[this.height][this.width];
			
			for (int i = 0; i < this.height; i++) {
				
				for (int j = 0; j < this.width; j++) {
					
					matrixClone[i][j] = matrix[i][j];
				}
			}
			
			for (int i = 0; i < this.height; i++) {
				
				for (int j = 0; j < this.width; j++) {
					
					int newY = j;
					int newX = this.width - (i + 1);
					
					matrix[i][j] = matrixClone[newY][newX];
				}
			}
		}
		
		for (int i = 0; i < this.height; i++) {
			
			for (int j = 0; j < this.width; j++) {
				
				this.pixels[(i * this.width) + j] = matrix[i][j];
			}
		}
	}
	
	private void load() {
		
		for (int y = 0; y < this.height; y++) {
			
			for (int x = 0; x < this.width; x++) {
				
				this.pixels[x + y * this.width] = this.sheet.pixels[(x + (this.x * this.width)) + (y + (this.y * this.width)) * this.sheet.width];
			}
		}
	}
	
	private void load(Color[] greyscale, Color[] colored) {
		
		for (int y = 0; y < this.height; y++) {
			
			for (int x = 0; x < this.width; x++) {
				
				int col = this.sheet.pixels[(x + (this.x * this.width)) + (y + (this.y * this.width)) * this.sheet.width];
				
				for (int i = 0; i < greyscale.length; i++) {
					
					Color c = greyscale[i];
					
					if (col == c.getRGB()) {
						
						col = colored[i].getRGB();
					}
				}
				
				this.pixels[x + y * this.width] = col;
			}
		}
	}
	
	public int getWidth() {
		
		return this.width;
	}
	
	public int getHeight() {
		
		return this.height;
	}
	
	public SpriteSheet getSpriteSheet() {
		
		return this.sheet;
	}
}