package com.anthony.foobar.game.graphics.screen;

import com.anthony.foobar.game.Game;
import com.anthony.foobar.game.graphics.sprite.Sprite;
import com.anthony.foobar.game.utils.UpDownInt;
import com.anthony.foobar.utils.ColorHandler;

public class Screen {
	
	private Game game;
	
	private int[] pixels;
	
	private UpDownInt brightness;
	
	public Screen(Game game) {
		
		this.game = game;
		
		this.pixels = new int[this.game.getWidth() * this.game.getHeight()];
		
		for (int i = 0; i < this.pixels.length; i++) {
			
			this.pixels[i] = 0xFF000000;
		}
		
		this.brightness = new UpDownInt(5, -100, 50);
	}
	
	public void tick() {
		
		this.brightness.tick();
	}
	
	public void clear() {
		
		for (int i = 0; i < this.pixels.length; i++) {
			
			this.pixels[i] = 0xFF000000;
		}
	}
	
	public void renderSprite(int xVal, int yVal, Sprite sprite) {
		
		this.renderSprite(xVal, yVal, 1, sprite);
	}
	
	
	public void renderSprite(int xVal, int yVal, int scale, Sprite sprite) {
		
		this.renderSprite(xVal, yVal, scale, false, sprite);
	}
	
	public void renderSprite(int xVal, int yVal, int scale, boolean samePixelAmount, Sprite sprite) {
		
		for (int y = 0; y < sprite.getHeight(); y++) {
			
			for (int yScale = 0; samePixelAmount ? yScale == 0 : yScale < scale; yScale++) {
				
				int ya = yVal + (y * scale) + (samePixelAmount ? 0 : yScale);
				
				for (int x = 0; x < sprite.getWidth(); x++) {
					
					for (int xScale = 0; samePixelAmount ? xScale == 0 : xScale < scale; xScale++) {
						
						int xa = xVal + (x * scale) + (samePixelAmount ? 0 : xScale);
						
						int col = sprite.pixels[x + y * sprite.getWidth()];
						
						if (xa < 0 || xa >= this.game.getWidth() || ya < 0 || ya >= this.game.getHeight()) {
							
							continue;
						}
						
						if (col != 0xffff00ff) {
							
							col = ColorHandler.changeBrightness(col, this.brightness.get());
							
							this.pixels[xa + ya * this.game.getWidth()] = col;
						}
					}
				}
			}
		}
	}
	
	public void renderBrightnessSprite(int brightness, boolean allowOtherBrightnesses, int xVal, int yVal, Sprite sprite) {
		
		for (int y = 0; y < sprite.getHeight(); y++) {
			
			int ya = y + yVal;
			
			for (int x = 0; x < sprite.getWidth(); x++) {
				
				int xa = x + xVal;
				
				int col = sprite.pixels[x + y * sprite.getWidth()];
				
				if (xa < 0 || xa >= this.game.getWidth() || ya < 0 || ya >= this.game.getHeight()) {
					
					continue;
				}
				
				if (col != 0xffff00ff) {
					
					col = ColorHandler.changeBrightness(col, brightness);
					
					if (allowOtherBrightnesses) {
						
						col = ColorHandler.changeBrightness(col, this.brightness.get());
					}
					
					this.pixels[xa + ya * this.game.getWidth()] = col;
				}
			}
		}
	}
	
	public void renderPortionSprite(int minX, int maxX, int minY, int maxY, int xVal, int yVal, Sprite sprite) {
		
		if (minX == maxX || minY == maxY) {
			
			return;
		}
		
		for (int y = 0; y < sprite.getHeight(); y++) {
			
			if (y < minY || y > maxY) {
				
				continue;
			}
			
			for (int x = 0; x < sprite.getWidth(); x++) {
				
				if (x < minX || x > maxX) {
					
					continue;
				}
				
				int ya = y + yVal;
				
				int xa = x + xVal;
				
				int col = sprite.pixels[x + y * sprite.getWidth()];
				
				if (xa < 0 || xa >= this.game.getWidth() || ya < 0 || ya >= this.game.getHeight()) {
					
					continue;
				}
				
				if (col != 0xffff00ff) {
					
					col = ColorHandler.changeBrightness(col, this.brightness.get());
					
					this.pixels[xa + ya * this.game.getWidth()] = col;
				}
			}
		}
	}
	
//	TODO renderTile ?
	
	public void setBrightness(int brightness) {
		
		this.brightness.setValue(brightness, true);
	}
	
	public int[] getPixels() {
		
		return this.pixels;
	}
}