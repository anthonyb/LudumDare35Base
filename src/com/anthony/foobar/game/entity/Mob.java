package com.anthony.foobar.game.entity;

import com.anthony.foobar.game.entity.type.Damageable;
import com.anthony.foobar.game.graphics.sprite.Sprite;

public class Mob extends Entity implements Damageable {
	
	protected int health, maxHealth;
	
	protected String identifier;
	
	protected Sprite sprite;
	
	public Mob(int maxHealth, int x, int y, String identifier, Sprite sprite) {
		
		super(x, y);
		
		this.health = this.maxHealth = maxHealth;
		
		this.identifier = identifier;
		
		this.sprite = sprite;
	}
	
	@Override
	public void remove() {
		
//		TODO
	}
	
	@Override
	public int getHealth() {
		
		return this.health;
	}
	
	@Override
	public int getMaxHealth() {
		
		return this.maxHealth;
	}
	
	@Override
	public void kill() {
		
		this.remove();
	}
	
	@Override
	public void setHealth(int health) {
		
		this.health = health;
	}
	
	@Override
	public void setMaxHealth(int maxHealth) {
		
		this.maxHealth = maxHealth;
	}
	
	public String getIdentifier() {
		
		return this.identifier;
	}
}