package com.anthony.foobar.game.entity;

public abstract class Entity {
	
	protected int x, y;
	
	public Entity(int x, int y) {
		
		this.x = x;
		this.y = y;
	}
	
	public abstract void remove();
}