package com.anthony.foobar.game.entity.type;

public interface Damageable {
	
	public abstract int getHealth();
	
	public abstract int getMaxHealth();
	
	public abstract void kill();
	
	public default void addHealth(int health) {
		
		int newHealth = this.getHealth() + Math.abs(health), maxHealth = this.getMaxHealth();
		
		this.setHealth(newHealth < 0 ? 0 : newHealth <= maxHealth ? newHealth : maxHealth);
	}
	
	public default void subtractHealth(int health) {
		
		this.addHealth(-Math.abs(health));
	}
	
	public abstract void setHealth(int health);
	
	public abstract void setMaxHealth(int maxHealth);
}
