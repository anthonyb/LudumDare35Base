package com.anthony.foobar.game.utils;

public class UpDownInt {
	
	private int rate, min, max, num, time;
	
	private boolean rising;
	
	public UpDownInt(int rate, int min, int max) {
		
		this(rate, min, max, 0, true);
	}
	
	public UpDownInt(int rate, int min, int max, int startingNum, boolean startRising) {
		
		this.rate = rate;
		this.min = min;
		this.max = max;
		this.num = startingNum;
		this.time = 0;
		
		this.rising = startRising;
	}
	
	public void tick() {
		
		this.time++;
		
		if (this.time == this.rate) {
			
			if (this.rising) {
				
				this.num++;
				
				if (this.num > this.max) {
					
					this.rising = false;
				}
			} else {
				
				this.num--;
				
				if (this.num < this.min) {
					
					this.rising = true;
				}
			}
			
			this.time = 0;
		}
	}
	
	public void setRate(int rate) {
		
		this.rate = rate;
	}
	
	public void setValue(int num, boolean startRising) {
		
		this.num = num;
		this.rising = rising;
	}
	
	public int get() {
		
		return this.num;
	}
}